package model;

/**
 * Created by Ioan on 25.04.2017.
 */
public class Customer {

    private Long customer_id;
    private String email;
    private Long age;
    private String name;

    public Customer(){
//        customer_id = 0;
//        email = "no email";
//        age = 0;
//        name = "no name";
    }

    public Long getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(Long customer_id) {
        this.customer_id = customer_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
