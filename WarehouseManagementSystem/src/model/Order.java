package model;

/**
 * Created by Ioan on 25.04.2017.
 */
public class Order {

    private Long order_id;
    private Long customer_id;

    public Order(Long order_id, Long customer_id) {
        this.order_id = order_id;
        this.customer_id = customer_id;
    }

    public Long getOrder_id() {
        return order_id;
    }

    public void setOrder_id(Long order_id) {
        this.order_id = order_id;
    }

    public Long getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(Long customer_id) {
        this.customer_id = customer_id;
    }
}
