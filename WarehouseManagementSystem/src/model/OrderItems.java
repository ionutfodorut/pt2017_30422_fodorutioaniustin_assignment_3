package model;

/**
 * Created by Ioan on 08.05.2017.
 */
public class OrderItems {

    private Long quantity;
    private Long product_id;
    private Long order_id;

    public OrderItems(Long quantity, Long product_id, Long order_id) {
        this.quantity = quantity;
        this.product_id = product_id;
        this.order_id = order_id;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }

    public Long getOrder_id() {
        return order_id;
    }

    public void setOrder_id(Long order_id) {
        this.order_id = order_id;
    }
}
