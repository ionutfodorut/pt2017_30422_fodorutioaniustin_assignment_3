package model;

/**
 * Created by Ioan on 25.04.2017.
 */
public class Product {
    private String name;
    private Long price;
    private Long stock;
    private Long product_id;

    public Product(String name, Long price, Long stock, Long product_id) {
        this.name = name;
        this.price = price;
        this.stock = stock;
        this.product_id = product_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }

    public Long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }
}
