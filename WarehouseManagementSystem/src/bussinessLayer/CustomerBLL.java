package bussinessLayer;

/**
 * Created by Ioan on 07.05.2017.
 */

import bussinessLayer.validators.CustomerAgeValidator;
import bussinessLayer.validators.EmailValidator;
import bussinessLayer.validators.Validator;
import dataAccessLayer.CustomerDAO;
import model.Customer;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class CustomerBLL {

    private List<Validator<Customer>> validators;
    private CustomerDAO CustomerDAO;

    public CustomerBLL() {
        validators = new ArrayList<Validator<Customer>>();
        validators.add(new EmailValidator());
        validators.add(new CustomerAgeValidator());

        CustomerDAO = new CustomerDAO();
    }

    public List<Customer> findAll() {
        List<Customer> st = CustomerDAO.findAll();
        if (st == null) {
            throw new NoSuchElementException("No customers in list!");
        }
        return st;
    }

    public Customer findCustomerById(int id) {
        Customer st = CustomerDAO.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The Customer with id =" + id + " was not found!");
        }
        return st;
    }

}
