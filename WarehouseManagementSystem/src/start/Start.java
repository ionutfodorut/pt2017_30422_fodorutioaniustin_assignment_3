package start;

import bussinessLayer.CustomerBLL;
import model.Customer;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Ioan on 07.05.2017.
 */
public class Start {
    protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());

    public static void main(String[] args) throws SQLException {

        CustomerBLL customerBll = new CustomerBLL();

        Customer customer;

        try {
            customer = customerBll.findCustomerById(1);
            System.out.println("\n"+customer.getName()+" "+customer.getEmail()+" "+customer.getAge());
            //customerBll.findAll();

        } catch (Exception ex) {
            LOGGER.log(Level.INFO, ex.getMessage());
        }

        // obtain field-value pairs for object through reflection
        //ReflectionExample.retrieveProperties(customer);

    }

}
